# -*- ispell-local-dictionary: "american" -*-
#+OPTIONS: ':nil *:t -:t ::t <:t H:4 \n:nil ^:nil arch:headline
#+OPTIONS: author:t broken-links:nil c:nil creator:nil
#+OPTIONS: d:(not "LOGBOOK") date:t e:t email:nil f:t inline:t num:t
#+OPTIONS: p:nil pri:nil prop:nil stat:t tags:nil tasks:t tex:t
#+OPTIONS: timestamp:t title:t toc:t todo:t |:t
#+title: Sampling and Quantization Illustration: Remembering Christoph Ueberhuber.
#+author: Christophe Pouzat
#+email: christophe.pouzat@math.unistra.fr
#+language: en
#+select_tags: export
#+exclude_tags: noexport
#+creator: Emacs 29.1 (Org mode 9.6.10)
#+STARTUP: indent
#+LaTeX_CLASS: xetex-eng-xtof
#+LaTeX_HEADER: \renewenvironment{verbatim}{\begin{alltt} \scriptsize \color{Bittersweet} \vspace{0.2cm} }{\vspace{0.2cm} \end{alltt} \normalsize \color{black}}
#+LaTeX_HEADER: \definecolor{lightcolor}{gray}{.55}
#+LaTeX_HEADER: \definecolor{shadecolor}{gray}{.95}
#+LATEX_HEADER: \addbibresource{sampling.bib}
#+LATEX_HEADER: \usemintedstyle{emacs}
#+PROPERTY: header-args :eval no-export
#+PROPERTY: header-args:python :results pp :session *ECG*

#+NAME: org-latex-set-up
#+BEGIN_SRC emacs-lisp :results silent :exports none
(add-to-list 'org-latex-classes
	     '("xetex-eng-xtof"
	       "\\documentclass[koma,11pt]{scrartcl}
                 \\usepackage{xunicode,fontspec,xltxtra}
                 \\usepackage{unicode-math}
                 \\usepackage[usenames,dvipsnames]{xcolor}
                 \\usepackage{graphicx,longtable,url,rotating}
                 \\usepackage{amsmath}
                 \\usepackage{amsfonts}
                 \\usepackage{amssymb}
                 \\usepackage{subfig}
                 \\usepackage{minted}
                 \\usemintedstyle{autumn}
                 \\usepackage{algpseudocode}
                 \\usepackage[backend=biber,style=authoryear-icomp,isbn=false,url=false,eprint=false,doi=true,note=false,natbib=true]{biblatex}
                 \\usepackage{alltt}
                 \\setromanfont[Mapping=tex-text]{FreeSerif}
                 \\setsansfont[Mapping=tex-text]{FreeSans}
                 \\setmonofont[Mapping=tex-text]{FreeMono}
                 \\setmathfont{Asana Math}            
                 [NO-DEFAULT-PACKAGES]
                 [EXTRA]
                 \\usepackage{hyperref}
                 \\hypersetup{xetex,colorlinks=true,pagebackref=true,urlcolor=orange,citecolor=brown}"
                 ("\\section{%s}" . "\\section*{%s}")
                 ("\\subsection{%s}" . "\\subsection*{%s}")
                 ("\\subsubsection{%s}" . "\\subsubsection*{%s}")
                 ("\\paragraph{%s}" . "\\paragraph*{%s}")
                 ("\\subparagraph{%s}" . "\\subparagraph*{%s}")))
(setq org-latex-listings 'minted)
(setq org-latex-minted-options
      '(("bgcolor" "shadecolor")
	("fontsize" "\\scriptsize")))
(setq org-latex-toc-command "\\tableofcontents\n\\pagebreak\n\\listoffigures\n\\pagebreak\n\n")
(setq org-latex-pdf-process
      '("xelatex -shell-escape -interaction nonstopmode -output-directory %o %f"
	"biber %b"
	;"pybtex %b" 
	"xelatex -shell-escape -interaction nonstopmode -output-directory %o %f" 
	"xelatex -shell-escape -interaction nonstopmode -output-directory %o %f"))
#+END_SRC

#+NAME: stderr-redirection
#+BEGIN_SRC emacs-lisp :exports none :results silent
;; Redirect stderr output to stdout so that it gets printed correctly (found on
;; http://kitchingroup.cheme.cmu.edu/blog/2015/01/04/Redirecting-stderr-in-org-mode-shell-blocks/
(setq org-babel-default-header-args:sh
      '((:prologue . "exec 2>&1") (:epilogue . ":"))
      )
(setq org-babel-use-quick-and-dirty-noweb-expansion t)
#+END_SRC

#+NAME: make-sure-python3-is-used
#+begin_src emacs-lisp :exports none :results silent
(setq org-babel-python-command "python3")
(setq python-shell-interpreter org-babel-python-command)
(setq org-src-preserve-indentation t)
#+end_src

* Introduction

This document generates figures illustrating what [[https://en.wikipedia.org/wiki/Sampling_(signal_processing)][sampling]] (in the signal processing, not statistical, sense) and [[https://en.wikipedia.org/wiki/Quantization_(signal_processing)][quantization]] are. It does that by reproducing two figures of Christoph Ueberhuber \citep[Fig. 8.2 and 8.4]{ueberhuber:97}. A portion of an ECG (electrocardiogram) is used to that end. We start with a real data set =aami3a= from the [[https://physionet.org/content/aami-ec13/1.0.0/][ANSI/AAMI EC13 Test Waveforms]], part of [[https://physionet.org/][Physionet]] \citep{goldberger.ary.ea:00}.

* Getting the data

There is a library, the [[https://physionet.org/content/wfdb/10.6.2/convert/#files-panel][WFDB Software Package]] \citep{moody.pollard.moody:21} developed by the =Physionet= project, [[https://wfdb.readthedocs.io/en/latest/index.html][interfaced]] with =Python= and I will assume here that it has been installed and that it is available.

We start by importing =wfdb= and =os=:
#+NAME: import-wfdb
#+begin_src python :results silent :tangle sampling_and_quantization.py
import wfdb
import os
#+end_src

We can next download the full =aami-ec13= database:
#+NAME: download-aami-ec13
#+begin_src python :results output 
wfdb.dl_database('aami-ec13', os.getcwd())
#+end_src

#+RESULTS:
#+begin_example
Generating record list for: aami3a
Generating record list for: aami3b
Generating record list for: aami3c
Generating record list for: aami3d
Generating record list for: aami4a
Generating record list for: aami4a_d
Generating record list for: aami4a_h
Generating record list for: aami4b
Generating record list for: aami4b_d
Generating record list for: aami4b_h
Generating list of all files for: aami3a
Generating list of all files for: aami3b
Generating list of all files for: aami3c
Generating list of all files for: aami3d
Generating list of all files for: aami4a
Generating list of all files for: aami4a_d
Generating list of all files for: aami4a_h
Generating list of all files for: aami4b
Generating list of all files for: aami4b_d
Generating list of all files for: aami4b_h
Downloading files...
Finished downloading files
#+end_example

We can more economically download the 2 files we really need from this database: =aami3a.dat= and =aami3a.hea= with:
#+NAME: download-aami3a
#+begin_src python :results output :tangle sampling_and_quantization.py :exports both
wfdb.dl_files('aami-ec13', os.getcwd(), ['aami3a.hea','aami3a.dat'])
#+end_src

#+RESULTS: download-aami3a
: Downloading files...
: Finished downloading files

We load next the header and the data in our =Python= session:
#+NAME: load-aami3a
#+begin_src python :results output :tangle sampling_and_quantization.py
ecg_header = wfdb.rdheader('aami3a')
ecg_record = wfdb.rdrecord('aami3a')
#+end_src

We can see what the first 3 seconds of data look like with (Fig. [[fig:ecg_3s]]):
#+NAME: plot-ecg_record
#+begin_src python :exports code :eval never
import matplotlib.pylab as plt
t3s = np.arange(0,3*720)/720
plt.plot(t3s,ecg_record.p_signal[:3*720])
plt.grid()
plt.xlabel("Time (s)")
plt.ylabel("ECG (mV)")
#+end_src

#+NAME: plot-ecg_record-interactive
#+begin_src python :exports none :noweb yes :results silent
<<plot-ecg_record>>
plt.show()
#+end_src

#+NAME: plot-ecg_record-out
#+HEADERS: :noweb yes :results file
#+begin_src python :exports results 
fname = 'ecg_3s.png'
<<plot-ecg_record>>
plt.savefig(fname)
plt.close()
fname
#+end_src

#+ATTR_LaTeX: :width 0.7\textwidth :float nil
#+LABEL: fig:ecg_3s
#+CAPTION[3 seconds of ECG data]: <<fig:ecg_3s>>First 3 seconds of dataset =aami3a= from database ANSI/AAMI EC13 Test Waveforms.
#+RESULTS: plot-ecg_record-out
[[file:ecg_3s.png]]

* Extracting the "interesting" part and making homogenization with splines

** Choosing an "interesting" part
We extract the part we are interested in (the first clearly bi-phasic wave):
#+NAME: extract-a-spike
#+begin_src python :results silent :tangle sampling_and_quantization.py
spike = ecg_record.p_signal[530:530+720]
#+end_src

** Smoothing the "interesting" part
We smooth it with smoothing splines:
#+NAME: smooth-spike
#+begin_src python :results silent :tangle sampling_and_quantization.py
import numpy as np
from scipy.interpolate import splrep, BSpline
tt = np.linspace(0, 1,720)
tck_s = splrep(tt, spike, s=0.1)
#+end_src

We can compare the orginal and smoothed data (Fig. [[fig:raw_and_smoot_spike]]):
#+NAME: raw-and-smooth-spike
#+begin_src python :exports code :eval never
plt.plot(tt,spike,lw=3)
plt.plot(tt, BSpline(*tck_s)(tt), '-',lw=2)
plt.grid()
plt.xlabel("Time (1 unit = 1/720 s)")
plt.ylabel("ECG (mV)")
#+end_src

#+NAME: raw-and-smooth-spike-interactive
#+begin_src python :noweb yes :exports none :results silent
<<raw-and-smooth-spike>>
plt.show()
#+end_src

#+NAME: raw-and-smooth-spike-out
#+HEADERS: :noweb yes :results file
#+begin_src python :exports results 
fname = 'raw_and_smoot_spike.png'
<<raw-and-smooth-spike>>
plt.savefig(fname)
plt.close()
fname
#+end_src

#+ATTR_LaTeX: :width 0.7\textwidth :float nil
#+LABEL: fig:raw_and_smoot_spike
#+CAPTION[Raw and smoothed ECG]: <<fig:raw_and_smoot_spike>>First bi-phasic wave raw and smoothed.
#+RESULTS: raw-and-smooth-spike-out
[[file:raw_and_smoot_spike.png]]

** Sampling the "interesting" part
Do sampling by taking only 20 observations per second instead of 720:
#+NAME: discretization-sampling
#+begin_src python :results silent :tangle sampling_and_quantization.py
ss = np.linspace(0.02, 1.02,20)
yy = BSpline(*tck_s)(ss)
#+end_src

This process is illustrated on Fig. [[fig:sampling-illustration]]:
#+NAME: sampling-illustration
#+begin_src python :exports code :eval never
plt.plot(tt, BSpline(*tck_s)(tt),'-',lw=2,color='blue')
plt.scatter(ss,BSpline(*tck_s)(ss),color='orange',s=81)
plt.vlines(ss,-0.5,0.5,color='gray')
plt.xlim([0,0.8])
plt.ylim([-0.5,0.5])
plt.xticks([])
plt.yticks([])
plt.xlabel("discretization (sampling)",fontsize=18)
plt.ylabel("ECG (mV)",fontsize=18)
#+end_src

#+NAME: sampling-illustration-interactive
#+begin_src python :exports none :noweb yes :results silent
<<sampling-illustration>>
plt.show()
#+end_src

#+NAME: sampling-illustration-out
#+HEADERS: :noweb yes :results file
#+begin_src python :exports results 
fname = 'sampling-illustration.png'
<<sampling-illustration>>
plt.savefig(fname)
plt.close()
fname
#+end_src

#+ATTR_LaTeX: :width 0.7\textwidth :float nil
#+LABEL: fig:sampling-illustration
#+CAPTION[Sampling illustration]: <<fig:sampling-illustration>>The signal (blue) line is discretized / sampled by keeping its value at a finite set of regularly spaced discrete time points (orange dots).
#+RESULTS: sampling-illustration-out
[[file:sampling-illustration.png]]

** Quantization
We define a quantization step of 0.05 mV and we do quantization by taking the discrete value of the quantization grid that is the closest to our smoothed trace at the sampled points:
#+NAME: quantization
#+begin_src python :results silent :tangle sampling_and_quantization.py
hh = np.linspace(-0.5,0.5,21)
qq = [hh[np.argmin(np.abs(hh-y))] for y in yy]
#+end_src

This process is illustrated on Fig. [[fig:quantization-illustration]]:
#+NAME: quantization-illustration
#+begin_src python :exports code :eval never
plt.plot(tt, BSpline(*tck_s)(tt),'--',lw=2,color='blue')
plt.scatter(ss,BSpline(*tck_s)(ss),color='blue',s=36)
plt.hlines(hh,0,1,color='gray')
plt.scatter(ss,qq,color='orange',s=81)
plt.xlim([0,0.8])
plt.ylim([0,0.5])
plt.xticks([])
plt.yticks([])
plt.xlabel("discretization (sampling)",fontsize=18)
plt.ylabel("quantization",fontsize=18)
#+end_src

#+NAME: quantization-illustration-interactive
#+begin_src python :exports none :noweb yes :results silent
<<quantization-illustration>>
plt.show()
#+end_src

#+NAME: quantization-illustration-out
#+HEADERS: :noweb yes :results file
#+begin_src python :exports results 
fname = 'quantization-illustration.png'
<<quantization-illustration>>
plt.savefig(fname)
plt.close()
fname
#+end_src

#+ATTR_LaTeX: :width 0.7\textwidth :float nil
#+LABEL: fig:quantization-illustration
#+CAPTION[Quantization illustration]: <<fig:quantization-illustration>>The true signal values at the sampled points (blue dots) get quantized (orange dots) by "taking the closest horizontal line".
#+RESULTS: quantization-illustration-out
[[file:quantization-illustration.png]]

** End result: the sampled and quantized signal

Our end result is a "replicate" of Fig. 8.2 of \citet{ueberhuber:97} (Fig. [[fig:fig_8p2_replicate]]):
#+NAME: fig_8p2_replicate
#+begin_src python :exports code :eval never
from scipy.interpolate import CubicSpline
cs = CubicSpline(ss, qq)
plt.plot(tt, BSpline(*tck_s)(tt),'--',lw=2,color='blue')
plt.scatter(ss,qq,color='orange',s=81)
plt.plot(tt,cs(tt),color='orange',lw=2)
plt.vlines(ss,-0.5,0.5,color='gray')
plt.hlines(hh,0,1,color='gray')
plt.xlim([0,0.8])
plt.ylim([-0.5,0.5])
plt.xticks([])
plt.yticks([])
plt.xlabel("discretization (sampling)",fontsize=18)
plt.ylabel("quantization",fontsize=18)
#+end_src

#+NAME: fig_8p2_replicate-interactive
#+begin_src python :exports none :noweb yes :results silent
<<fig_8p2_replicate>>
plt.show()
#+end_src

#+NAME: fig_8p2_replicate-out
#+HEADERS: :noweb yes :results file
#+begin_src python :exports results 
fname = 'fig_8p2_replicate.png'
<<fig_8p2_replicate>>
plt.savefig(fname)
plt.close()
fname
#+end_src

#+ATTR_LaTeX: :width 0.7\textwidth :float nil
#+LABEL: fig:fig_8p2_replicate
#+CAPTION[Sampled and quantized signal]: <<fig:fig_8p2_replicate>>Replicate of Fig. 8.2 of \citet{ueberhuber:97}. The continuous orange line is a cubic spline interpolation of the sampled and quantized data (orange dots).
#+RESULTS: fig_8p2_replicate-out
[[file:fig_8p2_replicate.png]]

\printbibliography
