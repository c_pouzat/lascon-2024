# Course material for LASCON 2024

This repository contains material relataed to my lectures and my talk given at the [Latin American School on Computational Neuroscience](http://www.sisne.org/lascon-ix) in 2024.

The associated [website](https://lascon-2024-c-pouzat-a3c28bec95c3ca0964b9f9735c22c607e509a9893e.gitlab.io/) is what most readers will be interested in.

This repository contains mainly source files making the site and the lectures sildes (in `PDF`).
