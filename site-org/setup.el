(setq org-export-html-postamble nil)
(require 'ox-publish)
(setq org-publish-project-alist
      '(
	("web-notes"
	 :base-directory "~/gitlab/lascon-2024/site-org/"
	 :base-extension "org"
	 :publishing-directory "~/gitlab/lascon-2024/public/"
	 :exclude "~"
	 :recursive t
	 :publishing-function org-html-publish-to-html
	 :headline-levels 4             ; Just the default for this project.
	 :auto-preamble t
	 )
	("web-static"
	 :base-directory "~/gitlab/lascon-2024/site-org/"
	 :base-extension "css\\|png\\|jpg\\|gif\\|pdf\\|code\\|data\\|dat\\|gz\\|txt\\|py\\|html\\|hdf5\\|npz\\|ipynb"
	 :publishing-directory "~/gitlab/lascon-2024/public/"
	 :exclude "~"
	 :recursive t
	 :publishing-function org-publish-attachment
	 )
	("web" :components ("web-notes" "web-static"))))
(setq org-export-babel-evaluate nil)
